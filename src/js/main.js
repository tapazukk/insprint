// jQuery
//= ../../node_modules/jquery/dist/jquery.min.js

// Popper
//= ../../node_modules/popper.js/dist/umd/popper.js

// Bootstrap 4
//= ../../node_modules/bootstrap/js/dist/util.js
//= ../../node_modules/bootstrap/js/dist/button.js
//= ../../node_modules/bootstrap/js/dist/carousel.js
//= ../../node_modules/bootstrap/js/dist/collapse.js
//= ../../node_modules/bootstrap/js/dist/dropdown.js
//= ../../node_modules/bootstrap/js/dist/scrollspy.js
//= ../../node_modules/bootstrap/js/dist/tab.js


// User js
//= carousel.js
//= user.js
